#include "flash_dev_all.h"
#include "x25/flash_dev_at25sf.h"
#include "x25/flash_dev_gd25q40e.h"
#include "x25/flash_dev_gd25q127.h"
#include "x25/flash_dev_n25q064a11.h"
#include "x25/flash_dev_p25q40sh.h"
#include "x25/flash_dev_xt25f32.h"
#include "x25/flash_dev_z25vq32.h"
#include "x25/flash_dev_zd25q16b.h"
#include "x25/flash_dev_w25q32jv.h"
#include "x25/flash_dev_w25q16jv.h"
#include "flash_dev_at25df.h"
#include "flash_dev_at45d011.h"
#include "flash_dev_at45db.h"
#include "flash_dev_sst25.h"
#include "flash_dev_sst26.h"


static const t_flash_info* f_info_list[] = {
    &flash_info_at25sf,
    &flash_info_gd25q40e,
    &flash_info_gd25q127,
    &flash_info_n25q064a11,
    &flash_info_p25q40sh,
    &flash_info_xt25f32,
    &flash_info_z25vq32,
    &flash_info_zd25q16b,
    &flash_info_w25q32jv,
    &flash_info_w25q16jv,
    &flash_info_at25df41a,
    &flash_info_at25df41b,
    &flash_info_at45d011,
    &flash_info_at45db642,
    &flash_info_at45db641,
    &flash_info_at45db041,
    &flash_info_at45db161e,
    &flash_info_at45db161d,
    &flash_info_sst25,
    &flash_info_sst26,
};

t_flash_errs flash_set_any(t_flash_iface *iface, t_flash_jedec_id_ext *rd_id) {
    return flash_set_from_list(iface, f_info_list, sizeof(f_info_list)/sizeof(f_info_list[0]), rd_id);
}
