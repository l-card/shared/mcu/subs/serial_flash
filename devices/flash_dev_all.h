#ifndef FLASH_DEV_ALL_H
#define FLASH_DEV_ALL_H

#include "../flash.h"

#ifdef __cplusplus
extern "C" {
#endif

t_flash_errs flash_set_any(t_flash_iface *iface, t_flash_jedec_id_ext *rd_id);

#ifdef __cplusplus
}
#endif

#endif // FLASH_DEV_ALL_H
