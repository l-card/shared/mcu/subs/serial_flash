#ifndef FLASH_DEV_AT25DF_H
#define FLASH_DEV_AT25DF_H


#include "../flash.h"

#ifdef __cplusplus
extern "C" {
#endif

#define AT25DF_ID_MANUFACTURER  0x1F
#define AT25DF_ID_DEVICE        0x44
#define AT25DF41A_ID_CAPACITY   0x01
#define AT25DF41B_ID_CAPACITY   0x02

#define AT25DF_FLASH_SIZE       (512*1024)


typedef enum {
    AT25DF_STATUS_BSY   = 1UL << 0, /* busy status */
    AT25DF_STATUS_WEL   = 1UL << 1, /* write enable latch */
    AT25DF_STATUS_SWP   = 3UL << 2, /* soft protect status */
    AT25DF_STATUS_WPP   = 1UL << 4, /* write protect pin status */
    AT25DF_STATUS_EPE   = 1UL << 5, /* erase/program error */
    AT25DF_STATUS_SPM   = 1UL << 6, /* sequential program mode */
    AT25DF_STATUS_SPRL  = 1UL << 7  /* sector protection registers locked */
} t_flash_at25df_status;

extern const t_flash_info flash_info_at25df41a;
extern const t_flash_info flash_info_at25df41b;

t_flash_errs flash_at25df_set(t_flash_iface *iface);


t_flash_errs flash_at25df_sector_protect(t_flash_iface *iface, unsigned addr);
t_flash_errs flash_at25df_sector_unprotect(t_flash_iface *iface, unsigned addr);
t_flash_errs flash_at25df_get_sector_protection(t_flash_iface *iface, unsigned addr, int *prot);

t_flash_errs flash_at25df_global_unprotect(t_flash_iface *iface);
t_flash_errs flash_at25df_global_protect(t_flash_iface *iface);


#define flash_at25df_get_status     flash_generic_get_status
#define flash_at25df_read           flash_generic_read
#define flash_at25df_erase          flash_generic_erase
#define flash_at25df_write          flash_generic_write
#define flash_at25df_write_enable   flash_generic_write_enable
#define flash_at25df_get_erase_cmd  flash_generic_get_erase_cmd
t_flash_errs flash_at25df_lock_enable(struct st_flash_iface *iface);
t_flash_errs flash_at25df_lock(struct st_flash_iface *iface, unsigned addr, unsigned size);
t_flash_errs flash_at25df_unlock(struct st_flash_iface *iface, unsigned addr, unsigned size);
t_flash_errs flash_at25df_lock_check(struct st_flash_iface *iface, unsigned addr, unsigned size, int locked, int *check_ok);

#ifdef __cplusplus
}
#endif

#endif // FLASH_DEV_AT25DF_H
