#ifndef FLASH_DEV_AT45D011_H
#define FLASH_DEV_AT45D011_H

#include "../flash.h"

#ifdef __cplusplus
extern "C" {
#endif

#define AT45D011_ID_MANUFACTURER  0x1F
#define AT45D011_ID_DEVICE        0x22
#define AT45D011_ID_CAPACITY      0x00

#define AT45D011_PAGE_SIZE        264
#define AT45D011_BLOCK_SIZE       (8*AT45D011_PAGE_SIZE)
#define AT45D011_SECTOR0_SIZE     AT45D011_BLOCK_SIZE
#define AT45D011_SECTOR1_SIZE     (AT45D011_SECTOR_SIZE - AT45D011_SECTOR0_SIZE)
#define AT45D011_SECTOR_SIZE      (32*AT45D011_BLOCK_SIZE)
#define AT45D011_SECTORS_CNT      3
#define AT45D011_FLASH_SIZE       (64 * AT45D011_BLOCK_SIZE)

#define AT45D011_CLK_MAX_FREQ     (15 * 1000000)


typedef enum {
    AT45D011_STATUS_RDY       = 1UL << 7,
    AT45D011_STATUS_COMP      = 1UL << 6
} t_flash_at45d011_status;


extern const t_flash_info flash_info_at45d011;

#define flash_at45d011_get_status  flash_generic_get_status
t_flash_errs flash_at45d011_read(t_flash_iface *iface, unsigned addr, unsigned char *data, size_t len);
t_flash_errs flash_at45d011_read_cb(t_flash_iface *iface, unsigned addr, unsigned char *data, size_t len, void (*cb)(void));
t_flash_errs flash_at45d011_erase(t_flash_iface *iface, unsigned addr, size_t size);
t_flash_errs flash_at45d011_erase_cb(t_flash_iface *iface, unsigned addr, size_t size, void (*cb)(void));
#define flash_at45d011_get_erase_cmd flash_generic_get_erase_cmd
t_flash_errs flash_at45d011_write(t_flash_iface *iface, unsigned addr,
                                const unsigned char *data, size_t len,
                                unsigned flags);
t_flash_errs flash_at45d011_write_cb(t_flash_iface *iface, unsigned addr,
                                const unsigned char *data, size_t len,
                                unsigned flags, void (*cb)(void));




#ifdef __cplusplus
}
#endif


#endif // FLASH_DEV_AT45D011_H
