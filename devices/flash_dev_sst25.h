#ifndef FLASH_DEV_SST25_H
#define FLASH_DEV_SST25_H

#include "../flash.h"

#ifdef __cplusplus
extern "C" {
#endif


#define SST25_ID_MANUFACTURER 0xBF
#define SST25_ID_DEVICE       0x25
#define SST25_ID_CAPACITY     0x41

#define SST25_FLASH_SIZE      (2*1024*1024)

extern const t_flash_info flash_info_sst25;


/** биты регистра статуса */
typedef enum {
    SST25_STATUS_BUSY   = 0x01,
    SST25_STATUS_WEL    = 0x02,
    SST25_STATUS_BP0    = 0x04,
    SST25_STATUS_BP1    = 0x08,
    SST25_STATUS_BP2    = 0x10,
    SST25_STATUS_BP3    = 0x20,
    SST25_STATUS_AAI    = 0x40,
    SST25_STATUS_BPL    = 0x80
} t_sst25_status_bits;


t_flash_errs flash_sst25_set(t_flash_iface *iface);

t_flash_errs flash_sst25_set_status(t_flash_iface *iface, unsigned char status);

#define flash_sst25_get_status flash_generic_get_status
#define flash_sst25_read       flash_generic_read
#define flash_sst25_erase      flash_generic_erase
t_flash_errs flash_sst25_write(t_flash_iface *iface, unsigned addr, const unsigned char* data, size_t len, unsigned flags);
#define flash_sst25_write_enable flash_generic_write_enable
#define flash_sst25_get_erase_cmd flash_generic_get_erase_cmd

#ifdef __cplusplus
}
#endif




#endif // FLASH_DEV_SST25_H
