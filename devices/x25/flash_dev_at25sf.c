#include "flash_dev_at25sf.h"



static const t_flash_erase_cmd f_erase_cmds[] = {
    {FLASH_CMD_CODE_ERASE_CHIP, AT25SF_FLASH_SIZE,  AT25SF_ERASE_CHIP_TIME_MS, FLASH_ERASE_CMD_FLAG_NOADDR},
    {FLASH_CMD_CODE_ERASE_64K,            64*1024,  AT25SF_ERASE_64K_TIME_MS,  0},
    {FLASH_CMD_CODE_ERASE_32K,            32*1024,  AT25SF_ERASE_32K_TIME_MS,  0},
    {FLASH_CMD_CODE_ERASE_4K,                4096,  AT25SF_ERASE_4K_TIME_MS,   0},
    {0,0,0,0}
};

const t_flash_info flash_info_at25sf =  {
    {AT25SF_ID_MANUFACTURER, AT25SF_ID_DEVICE, AT25SF_ID_CAPACITY, FLASH_ID_EDI_LEN_NOT_SUPPORTED},
    AT25SF_FLASH_SIZE,
    {FLASH_CMD_CODE_READ_STATUS, FLASH_X25_STATUS_BSY, 0x0, 0x0},
    {FLASH_CMD_CODE_READ, 0},
    {f_erase_cmds, FLASH_CMD_CODE_WRITE_ENABLE},
    {0x100, FLASH_CMD_CODE_PAGE_PROGRAM, AT25SF_PAGE_PROG_TIME_US},
    {0},
    {AT25SF_STATUS_LEN, AT25SF_STATUS_MASK, AT25SF_STATUS_WR_TIME_MS},
    {   NULL,
        flash_at25sf_get_status,
        flash_at25sf_read,
        flash_at25sf_erase,
        flash_at25sf_get_erase_cmd,
        flash_at25sf_write,
        flash_at25sf_write_enable,
        NULL,
        NULL,
        NULL,
        NULL,

        NULL,
        NULL,
        NULL,

        NULL,
        flash_at25sf_get_status_ex,
        flash_at25sf_set_status_ex
    },
    NULL
};
