#ifndef FLASH_DEV_AT25SF_H
#define FLASH_DEV_AT25SF_H


#include "flash_dev_x25.h"

#ifdef __cplusplus
extern "C" {
#endif


#define AT25SF_ID_MANUFACTURER  0x1F
#define AT25SF_ID_DEVICE        0x84
#define AT25SF_ID_CAPACITY      0x01

#define AT25SF_FLASH_SIZE       (512*1024)


#define AT25SF_ERASE_CHIP_TIME_MS              10000
#define AT25SF_ERASE_64K_TIME_MS               2200
#define AT25SF_ERASE_32K_TIME_MS               1300
#define AT25SF_ERASE_4K_TIME_MS                300
#define AT25SF_PAGE_PROG_TIME_US               2500
#define AT25SF_STATUS_WR_TIME_MS               15


#define AT25SF_STATUS_LEN                      2
#define AT25SF_STATUS_MASK                     (FLASH_X25_STATUS_BSY \
                                                | FLASH_X25_STATUS_WEL \
                                                | FLASH_X25_STATUS_BP0 \
                                                | FLASH_X25_STATUS_BP1 \
                                                | FLASH_X25_STATUS_BP2 \
                                                | FLASH_X25_STATUS_BP3 \
                                                | FLASH_X25_STATUS_BP4 \
                                                | FLASH_X25_STATUS_SRP0 \
                                                | FLASH_X25_STATUS_SRP1 \
                                                | FLASH_X25_STATUS_QE \
                                                | FLASH_X25_STATUS_LB1 \
                                                | FLASH_X25_STATUS_LB2 \
                                                | FLASH_X25_STATUS_LB3 \
                                                | FLASH_X25_STATUS_CMP \
                                                )


extern const t_flash_info flash_info_at25sf;

t_flash_errs flash_at25sf_set(t_flash_iface *iface);

#define flash_at25sf_get_status     flash_generic_get_status
#define flash_at25sf_read           flash_generic_read
#define flash_at25sf_erase          flash_generic_erase
#define flash_at25sf_write          flash_generic_write
#define flash_at25sf_write_enable   flash_generic_write_enable
#define flash_at25sf_get_erase_cmd  flash_generic_get_erase_cmd

#define flash_at25sf_get_status_ex  flash_x25_get_status16_2cmd
#define flash_at25sf_set_status_ex  flash_x25_set_status16_1cmd

#ifdef __cplusplus
}
#endif


#endif // FLASH_DEV_AT25SF_H

