#ifndef FLASH_DEV_GD25Q127_H
#define FLASH_DEV_GD25Q127_H

#include "flash_dev_x25.h"

#ifdef __cplusplus
extern "C" {
#endif

#define GD25Q127_ID_MANUFACTURER        0xC8
#define GD25Q127_ID_DEVICE              0x40
#define GD25Q127_ID_CAPACITY            0x18

#define GD25Q127_FLASH_SIZE             (16*1024*1024)
#define GD25Q127_PAGE_SIZE              256
#define GD25Q127_SECTOR_SIZE            4096

#define GD25Q127_UID_BITLEN               128

#define GD25Q127_ERASE_CHIP_TIME_MS      (120*1000)
#define GD25Q127_ERASE_64K_TIME_MS       1200
#define GD25Q127_ERASE_32K_TIME_MS        800
#define GD25Q127_ERASE_4K_TIME_MS         400
#define GD25Q127_PAGE_PROG_TIME_US       2400
#define GD25Q127_STATUS_WR_TIME_MS         30


#define GD25Q127_STATUS_LEN                    3
#define GD25Q127_STATUS_MASK                   (FLASH_X25_STATUS_BSY \
                                                | FLASH_X25_STATUS_WEL \
                                                | FLASH_X25_STATUS_BP0 \
                                                | FLASH_X25_STATUS_BP1 \
                                                | FLASH_X25_STATUS_BP2 \
                                                | FLASH_X25_STATUS_BP3 \
                                                | FLASH_X25_STATUS_BP4 \
                                                | FLASH_X25_STATUS_SRP0 \
                                                | FLASH_X25_STATUS_SRP1 \
                                                | FLASH_X25_STATUS_QE \
                                                | FLASH_X25_STATUS_SUS2 \
                                                | FLASH_X25_STATUS_LB1 \
                                                | FLASH_X25_STATUS_LB2 \
                                                | FLASH_X25_STATUS_LB3 \
                                                | FLASH_X25_STATUS_CMP \
                                                | FLASH_X25_STATUS_SUS1 \
                                                | FLASH_X25_STATUS_LPE \
                                                | FLASH_X25_STATUS_DRV2 \
                                                | FLASH_X25_STATUS_HRSW \
                                                )



extern const t_flash_info flash_info_gd25q127;


#define flash_gd25q127_get_status                   flash_generic_get_status
#define flash_gd25q127_read                         flash_generic_read
#define flash_gd25q127_erase                        flash_generic_erase
#define flash_gd25q127_write                        flash_generic_write
#define flash_gd25q127_write_enable                 flash_generic_write_enable
#define flash_gd25q127_get_erase_cmd                flash_generic_get_erase_cmd


#define flash_gd25q127_get_status_ex                flash_x25_get_status24_3cmd
#define flash_gd25q127_set_status_ex                flash_x25_set_status24_3cmd
#define flash_gd25q127_read_uid                     flash_x25_read_uid

#ifdef __cplusplus
}
#endif

#endif // FLASH_DEV_GD25Q127_H
