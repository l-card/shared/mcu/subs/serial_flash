#include "flash_dev_gd25q40e.h"

static const t_flash_erase_cmd f_gd25q40e_erase_cmds[] = {
    {FLASH_CMD_CODE_ERASE_CHIP,    GD25Q40E_FLASH_SIZE,  GD25Q40E_ERASE_CHIP_TIME_MS, FLASH_ERASE_CMD_FLAG_NOADDR},
    {FLASH_CMD_CODE_ERASE_64K,                64*1024,  GD25Q40E_ERASE_64K_TIME_MS,  0},
    {FLASH_CMD_CODE_ERASE_32K,                32*1024,  GD25Q40E_ERASE_32K_TIME_MS,  0},
    {FLASH_CMD_CODE_ERASE_4K,                    4096,  GD25Q40E_ERASE_4K_TIME_MS,   0},
    {0,0,0,0}
};



const t_flash_info flash_info_gd25q40e =  {
    {GD25Q40E_ID_MANUFACTURER, GD25Q40E_ID_DEVICE, GD25Q40E_ID_CAPACITY, FLASH_ID_EDI_LEN_NOT_SUPPORTED},
    GD25Q40E_FLASH_SIZE,
    {FLASH_CMD_CODE_READ_STATUS, FLASH_X25_STATUS_BSY, 0x0, 0x0},
    {FLASH_CMD_CODE_READ, 0},
    {f_gd25q40e_erase_cmds, FLASH_CMD_CODE_WRITE_ENABLE},
    {GD25Q40E_PAGE_SIZE, FLASH_CMD_CODE_PAGE_PROGRAM, GD25Q40E_PAGE_PROG_TIME_US},
    {GD25Q40E_UID_BITLEN},
    {GD25Q40E_STATUS_LEN, GD25Q40E_STATUS_MASK, GD25Q40E_STATUS_WR_TIME_MS},
    {   NULL,
        flash_gd25q40e_get_status,
        flash_gd25q40e_read,
        flash_gd25q40e_erase,
        flash_gd25q40e_get_erase_cmd,
        flash_gd25q40e_write,
        flash_gd25q40e_write_enable,
        NULL,
        NULL,
        NULL,
        NULL,

        NULL,
        NULL,
        NULL,

        flash_gd25q40e_read_uid,
        flash_gd25q40e_get_status_ex,
        flash_gd25q40e_set_status_ex
    },
    NULL
};

