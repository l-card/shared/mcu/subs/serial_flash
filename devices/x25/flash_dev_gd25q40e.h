#ifndef FLASH_DEV_GD25Q40E_H
#define FLASH_DEV_GD25Q40E_H

#include "flash_dev_x25.h"

#ifdef __cplusplus
extern "C" {
#endif

#define GD25Q40E_ID_MANUFACTURER        0xC8
#define GD25Q40E_ID_DEVICE              0x65
#define GD25Q40E_ID_CAPACITY            0x13

#define GD25Q40E_FLASH_SIZE             (512*1024)
#define GD25Q40E_PAGE_SIZE              256
#define GD25Q40E_SECTOR_SIZE            4096

#define GD25Q40E_UID_BITLEN               128

#define GD25Q40E_ERASE_CHIP_TIME_MS      8000
#define GD25Q40E_ERASE_64K_TIME_MS       3000
#define GD25Q40E_ERASE_32K_TIME_MS       2000
#define GD25Q40E_ERASE_4K_TIME_MS         500
#define GD25Q40E_PAGE_PROG_TIME_US        120
#define GD25Q40E_STATUS_WR_TIME_MS         30

#define GD25Q40E_STATUS_LEN                    2
#define GD25Q40E_STATUS_MASK                   (FLASH_X25_STATUS_BSY \
                                                | FLASH_X25_STATUS_WEL \
                                                | FLASH_X25_STATUS_BP0 \
                                                | FLASH_X25_STATUS_BP1 \
                                                | FLASH_X25_STATUS_BP2 \
                                                | FLASH_X25_STATUS_BP3 \
                                                | FLASH_X25_STATUS_BP4 \
                                                | FLASH_X25_STATUS_SRP0 \
                                                | FLASH_X25_STATUS_SRP1 \
                                                | FLASH_X25_STATUS_QE \
                                                | FLASH_X25_STATUS_LB0 \
                                                | FLASH_X25_STATUS_LB1 \
                                                | FLASH_X25_STATUS_CMP \
                                                | FLASH_X25_STATUS_SUS1 \
                                                )


extern const t_flash_info flash_info_gd25q40e;


#define flash_gd25q40e_get_status               flash_generic_get_status
#define flash_gd25q40e_read                     flash_generic_read
#define flash_gd25q40e_erase                    flash_generic_erase
#define flash_gd25q40e_write                    flash_generic_write
#define flash_gd25q40e_write_enable             flash_generic_write_enable
#define flash_gd25q40e_get_erase_cmd            flash_generic_get_erase_cmd


#define flash_gd25q40e_get_status_ex            flash_x25_get_status16_2cmd
#define flash_gd25q40e_set_status_ex            flash_x25_set_status16_1cmd
#define flash_gd25q40e_read_uid                 flash_x25_read_uid

#ifdef __cplusplus
}
#endif

#endif // FLASH_DEV_GD25Q40E_H
