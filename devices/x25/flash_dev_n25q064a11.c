#include "flash_dev_n25q064a11.h"





static const t_flash_erase_cmd f_erase_cmds[] = {
    {FLASH_CMD_CODE_ERASE_64K, 64*1024, 3000, 0},
    {FLASH_CMD_CODE_ERASE_4K,  4*1024,  3000, 0},
    {0,0,0,0}
};

const t_flash_info flash_info_n25q064a11 =  {
    {N25Q064A11_ID_MANUFACTURER, N25Q064A11_ID_DEVICE, N25Q064A11_ID_CAPACITY, 0x10},
    N25Q064A11_FLASH_SIZE,
    {FLASH_CMD_CODE_READ_STATUS, N25Q064A11_STATUS_BUSY, 0x0,0x0},
    {FLASH_CMD_CODE_READ, 0},
    {f_erase_cmds, FLASH_CMD_CODE_WRITE_ENABLE},
    {256, FLASH_CMD_CODE_PAGE_PROGRAM, 5000},
    {0},
    {0,0,0},
    {   NULL,
        flash_n25q064a11_get_status,
        flash_n25q064a11_read,
        flash_n25q064a11_erase,
        flash_generic_get_erase_cmd,
        flash_n25q064a11_write,
        flash_n25q064a11_write_enable,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL
    },
    NULL
};


t_flash_errs flash_n25q064a11_set(t_flash_iface *iface) {
    return flash_set(iface, &flash_info_n25q064a11, NULL);
}
