#ifndef FLASH_DEV_N25Q064A11_H
#define FLASH_DEV_N25Q064A11_H

#include "../../flash.h"

#ifdef __cplusplus
extern "C" {
#endif

#define N25Q064A11_ID_MANUFACTURER  0x20
#define N25Q064A11_ID_DEVICE        0xBB
#define N25Q064A11_ID_CAPACITY      0x17

#define N25Q064A11_FLASH_SIZE       (8*1024*1024)


/** биты регистра статуса */
typedef enum {
    N25Q064A11_STATUS_BUSY   = 0x01,
    N25Q064A11_STATUS_WEL    = 0x02,
    N25Q064A11_STATUS_BP0    = 0x04,
    N25Q064A11_STATUS_BP1    = 0x08,
    N25Q064A11_STATUS_BP2    = 0x10,
    N25Q064A11_STATUS_BP3    = 0x20,
    N25Q064A11_STATUS_BOTTOM = 0x40, /* protect start from top/bottom */
    N25Q064A11_STATUS_SRWE   = 0x80  /* status write protect */
} t_n25q064a11_status_bits;

extern const t_flash_info flash_info_n25q064a11;

t_flash_errs flash_n25q064a11_set(t_flash_iface *iface);

#define flash_n25q064a11_get_status     flash_generic_get_status
#define flash_n25q064a11_read           flash_generic_read
#define flash_n25q064a11_erase          flash_generic_erase
#define flash_n25q064a11_get_erase_cmd  flash_generic_get_erase_cmd
#define flash_n25q064a11_write          flash_generic_write
#define flash_n25q064a11_write_enable   flash_generic_write_enable


#ifdef __cplusplus
}
#endif


#endif // FLASH_DEV_N25Q064A11_H
