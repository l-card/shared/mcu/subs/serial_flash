#include "flash_dev_p25q40sh.h"

static const t_flash_erase_cmd f_p25q40sh_erase_cmds[] = {
    {FLASH_CMD_CODE_ERASE_CHIP,   P25Q40SH_FLASH_SIZE,  P25Q40SH_ERASE_CHIP_TIME_MS, FLASH_ERASE_CMD_FLAG_NOADDR},
    {FLASH_CMD_CODE_ERASE_64K,                64*1024,  P25Q40SH_ERASE_64K_TIME_MS,  0},
    {FLASH_CMD_CODE_ERASE_32K,                32*1024,  P25Q40SH_ERASE_32K_TIME_MS,  0},
    {FLASH_CMD_CODE_ERASE_4K,                    4096,  P25Q40SH_ERASE_4K_TIME_MS,   0},
    {FLASH_CMD_CODE_ERASE_PAGE,                   256,  P25Q40SH_ERASE_PAGE_TIME_MS, 0},
    {0,0,0,0}
};



const t_flash_info flash_info_p25q40sh =  {
    {P25Q40SH_ID_MANUFACTURER, P25Q40SH_ID_DEVICE, P25Q40SH_ID_CAPACITY, FLASH_ID_EDI_LEN_NOT_SUPPORTED},
    P25Q40SH_FLASH_SIZE,
    {FLASH_CMD_CODE_READ_STATUS, FLASH_X25_STATUS_BSY, 0x0, 0x0},
    {FLASH_CMD_CODE_READ, 0},
    {f_p25q40sh_erase_cmds, FLASH_CMD_CODE_WRITE_ENABLE},
    {P25Q40SH_PAGE_SIZE, FLASH_CMD_CODE_PAGE_PROGRAM, P25Q40SH_PAGE_PROG_TIME_US},
    {P25Q40SH_UID_BITLEN},
    {P25Q40SH_STATUS_LEN, P25Q40SH_STATUS_MASK, P25Q40SH_STATUS_WR_TIME_MS},
    {   NULL,
        flash_p25q40sh_get_status,
        flash_p25q40sh_read,
        flash_p25q40sh_erase,
        flash_p25q40sh_get_erase_cmd,
        flash_p25q40sh_write,
        flash_p25q40sh_write_enable,
        NULL,
        NULL,
        NULL,
        NULL,

        NULL,
        NULL,
        NULL,

        flash_p25q40sh_read_uid,
        flash_p25q40sh_get_status_ex,
        flash_p25q40sh_set_status_ex
    },
    NULL
};

