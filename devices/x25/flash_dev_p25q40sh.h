#ifndef FLASH_DEV_P25Q40SH_H
#define FLASH_DEV_P25Q40SH_H


#include "flash_dev_x25.h"

#ifdef __cplusplus
extern "C" {
#endif

#define P25Q40SH_ID_MANUFACTURER         0x85
#define P25Q40SH_ID_DEVICE               0x60
#define P25Q40SH_ID_CAPACITY             0x13

#define P25Q40SH_FLASH_SIZE              (512*1024)
#define P25Q40SH_PAGE_SIZE               256
#define P25Q40SH_SECTOR_SIZE             4096

#define P25Q40SH_UID_BITLEN              128

#define P25Q40SH_ERASE_CHIP_TIME_MS            30
#define P25Q40SH_ERASE_64K_TIME_MS             30
#define P25Q40SH_ERASE_32K_TIME_MS             30
#define P25Q40SH_ERASE_4K_TIME_MS              30
#define P25Q40SH_ERASE_PAGE_TIME_MS            30
#define P25Q40SH_PAGE_PROG_TIME_US        (3*1000)
#define P25Q40SH_STATUS_WR_TIME_MS             12


#define P25Q40SH_STATUS_LEN                    2
#define P25Q40SH_STATUS_MASK                   (FLASH_X25_STATUS_BSY \
                                                | FLASH_X25_STATUS_WEL \
                                                | FLASH_X25_STATUS_BP0 \
                                                | FLASH_X25_STATUS_BP1 \
                                                | FLASH_X25_STATUS_BP2 \
                                                | FLASH_X25_STATUS_BP3 \
                                                | FLASH_X25_STATUS_BP4 \
                                                | FLASH_X25_STATUS_SRP0 \
                                                | FLASH_X25_STATUS_SRP1 \
                                                | FLASH_X25_STATUS_QE \
                                                | FLASH_X25_STATUS_EP_FAIL \
                                                | FLASH_X25_STATUS_LB1 \
                                                | FLASH_X25_STATUS_LB2 \
                                                | FLASH_X25_STATUS_LB3 \
                                                | FLASH_X25_STATUS_CMP \
                                                | FLASH_X25_STATUS_SUS1 \
                                                )


extern const t_flash_info flash_info_p25q40sh;


#define flash_p25q40sh_get_status     flash_generic_get_status
#define flash_p25q40sh_read           flash_generic_read
#define flash_p25q40sh_erase          flash_generic_erase
#define flash_p25q40sh_write          flash_generic_write
#define flash_p25q40sh_write_enable   flash_generic_write_enable
#define flash_p25q40sh_get_erase_cmd  flash_generic_get_erase_cmd

#define flash_p25q40sh_read_uid       flash_x25_read_uid
#define flash_p25q40sh_get_status_ex  flash_x25_get_status16_2cmd
#define flash_p25q40sh_set_status_ex  flash_x25_set_status16_1cmd

#ifdef __cplusplus
}
#endif

#endif

