#include "flash_dev_w25q16jv.h"

static const t_flash_erase_cmd f_w25q16_erase_cmds[] = {
    {FLASH_CMD_CODE_ERASE_CHIP,    W25Q16_FLASH_SIZE,  W25Q16_ERASE_CHIP_TIME_MS, FLASH_ERASE_CMD_FLAG_NOADDR},
    {FLASH_CMD_CODE_ERASE_64K,                64*1024,  W25Q16_ERASE_64K_TIME_MS,  0},
    {FLASH_CMD_CODE_ERASE_32K,                32*1024,  W25Q16_ERASE_32K_TIME_MS,  0},
    {FLASH_CMD_CODE_ERASE_4K,                    4096,  W25Q16_ERASE_4K_TIME_MS,   0},
    {0,0,0,0}
};



const t_flash_info flash_info_w25q16jv =  {
    {W25Q16_ID_MANUFACTURER, W25Q16_ID_DEVICE, W25Q16_ID_CAPACITY, FLASH_ID_EDI_LEN_NOT_SUPPORTED},
    W25Q16_FLASH_SIZE,
    {FLASH_CMD_CODE_READ_STATUS, FLASH_X25_STATUS_BSY, 0x0, 0x0},
    {FLASH_CMD_CODE_READ, 0},
    {f_w25q16_erase_cmds, FLASH_CMD_CODE_WRITE_ENABLE},
    {W25Q16_PAGE_SIZE, FLASH_CMD_CODE_PAGE_PROGRAM, W25Q16_PAGE_PROG_TIME_US},
    {W25Q16_UID_BITLEN},
    {W25Q16_STATUS_LEN, W25Q16_STATUS_MASK, W25Q16_STATUS_WR_TIME_MS},
    {   NULL,
        flash_w25q16_get_status,
        flash_w25q16_read,
        flash_w25q16_erase,
        flash_w25q16_get_erase_cmd,
        flash_w25q16_write,
        flash_w25q16_write_enable,
        NULL,
        NULL,
        NULL,
        NULL,

        NULL,
        NULL,
        NULL,

        flash_w25q16_read_uid,
        flash_w25q16_get_status_ex,
        flash_w25q16_set_status_ex
    },
    NULL
};
