#ifndef FLASH_DEV_W25Q16JV_H
#define FLASH_DEV_W25Q16JV_H

#include "flash_dev_x25.h"

#ifdef __cplusplus
extern "C" {
#endif

#define W25Q16_ID_MANUFACTURER         0xEF
#define W25Q16_ID_DEVICE               0x40
#define W25Q16_ID_CAPACITY             0x15

#define W25Q16_FLASH_SIZE              (1*1024*1024)
#define W25Q16_PAGE_SIZE               256
#define W25Q16_SECTOR_SIZE             4096

#define W25Q16_UID_BITLEN              64

#define W25Q16_ERASE_CHIP_TIME_MS      (25*1000)
#define W25Q16_ERASE_64K_TIME_MS        (2*1000)
#define W25Q16_ERASE_32K_TIME_MS           1600
#define W25Q16_ERASE_4K_TIME_MS             400
#define W25Q16_PAGE_PROG_TIME_US        (3*1000)
#define W25Q16_STATUS_WR_TIME_MS            15

#define W25Q16_STATUS_LEN                     3
#define W25Q16_STATUS_MASK                    (FLASH_X25_STATUS_BSY \
                                                | FLASH_X25_STATUS_WEL \
                                                | FLASH_X25_STATUS_BP0 \
                                                | FLASH_X25_STATUS_BP1 \
                                                | FLASH_X25_STATUS_BP2 \
                                                | FLASH_X25_STATUS_TB \
                                                | FLASH_X25_STATUS_SEC \
                                                | FLASH_X25_STATUS_SRP0 \
                                                | FLASH_X25_STATUS_SRL \
                                                | FLASH_X25_STATUS_QE \
                                                | FLASH_X25_STATUS_LB1 \
                                                | FLASH_X25_STATUS_LB2 \
                                                | FLASH_X25_STATUS_LB3 \
                                                | FLASH_X25_STATUS_CMP \
                                                | FLASH_X25_STATUS_SUS1 \
                                                | FLASH_X25_STATUS_WPS \
                                                | FLASH_X25_STATUS_DRV2 \
                                                | FLASH_X25_STATUS_DRV1 \
                                                )




extern const t_flash_info flash_info_w25q16jv;


#define flash_w25q16_get_status     flash_generic_get_status
#define flash_w25q16_read           flash_generic_read
#define flash_w25q16_erase          flash_generic_erase
#define flash_w25q16_write          flash_generic_write
#define flash_w25q16_write_enable   flash_generic_write_enable
#define flash_w25q16_get_erase_cmd  flash_generic_get_erase_cmd

#define flash_w25q16_get_status_ex  flash_x25_get_status24_3cmd
#define flash_w25q16_set_status_ex  flash_x25_set_status24_3cmd
#define flash_w25q16_read_uid       flash_x25_read_uid


#endif // FLASH_DEV_W25Q16JV_H
