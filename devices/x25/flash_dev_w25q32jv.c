#include "flash_dev_w25q32jv.h"

static const t_flash_erase_cmd f_w25q32_erase_cmds[] = {
    {FLASH_CMD_CODE_ERASE_CHIP,    W25Q32_FLASH_SIZE,  W25Q32_ERASE_CHIP_TIME_MS, FLASH_ERASE_CMD_FLAG_NOADDR},
    {FLASH_CMD_CODE_ERASE_64K,                64*1024,  W25Q32_ERASE_64K_TIME_MS,  0},
    {FLASH_CMD_CODE_ERASE_32K,                32*1024,  W25Q32_ERASE_32K_TIME_MS,  0},
    {FLASH_CMD_CODE_ERASE_4K,                    4096,  W25Q32_ERASE_4K_TIME_MS,   0},
    {0,0,0,0}
};



const t_flash_info flash_info_w25q32jv =  {
    {W25Q32_ID_MANUFACTURER, W25Q32_ID_DEVICE, W25Q32_ID_CAPACITY, FLASH_ID_EDI_LEN_NOT_SUPPORTED},
    W25Q32_FLASH_SIZE,
    {FLASH_CMD_CODE_READ_STATUS, FLASH_X25_STATUS_BSY, 0x0, 0x0},
    {FLASH_CMD_CODE_READ, 0},
    {f_w25q32_erase_cmds, FLASH_CMD_CODE_WRITE_ENABLE},
    {W25Q32_PAGE_SIZE, FLASH_CMD_CODE_PAGE_PROGRAM, W25Q32_PAGE_PROG_TIME_US},
    {W25Q32_UID_BITLEN},
    {W25Q32_STATUS_LEN, W25Q32_STATUS_MASK, W25Q32_STATUS_WR_TIME_MS},
    {   NULL,
        flash_w25q32_get_status,
        flash_w25q32_read,
        flash_w25q32_erase,
        flash_w25q32_get_erase_cmd,
        flash_w25q32_write,
        flash_w25q32_write_enable,
        NULL,
        NULL,
        NULL,
        NULL,

        NULL,
        NULL,
        NULL,

        flash_w25q32_read_uid,
        flash_w25q32_get_status_ex,
        flash_w25q32_set_status_ex
    },
    NULL
};
