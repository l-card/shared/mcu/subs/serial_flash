#ifndef FLASH_DEV_FLASH_X25_H
#define FLASH_DEV_FLASH_X25_H

#include "../../flash.h"

/** Коды команд, общих для большого числа вариантов памяти */
typedef enum {
    FLASH_X25_CMD_WRITE_STATUS3             = 0x11,
    FLASH_X25_CMD_READ_STATUS3              = 0x15, /* 33 */
    FLASH_X25_CMD_WRITE_STATUS2             = 0x31,
    FLASH_X25_CMD_READ_STATUS2              = 0x35,
    FLASH_X25_CMD_CMD_READ_UID              = 0x4B, /* часть моделей не имеют команды и читают UID через
                                                       определенный адрес таблиц SFDP */
    FLASH_X25_CMD_WR_EN_STATUS_VOLATILE     = 0x50,
} t_flash_x25_cmds;




typedef enum {
    FLASH_X25_STATUS_BSY        = 1UL << 0,             /* busy status */
    FLASH_X25_STATUS_WEL        = 1UL << 1,             /* write enable latch */
    FLASH_X25_STATUS_BP0        = 1UL << 2,             /* block protection bit-1 */
    FLASH_X25_STATUS_BP1        = 1UL << 3,             /* block protection bit-2 */
    FLASH_X25_STATUS_BP2        = 1UL << 4,             /* block protection bit-2 */
    FLASH_X25_STATUS_TB         = 1UL << 5,             /* top or bottom protection */
    FLASH_X25_STATUS_BP3        = FLASH_X25_STATUS_TB,  /* top/bottom protect*/
    FLASH_X25_STATUS_SEC        = 1UL << 6,             /* protection block size (0 - 64 kB, 1 - 4 kB)*/
    FLASH_X25_STATUS_BP4        = FLASH_X25_STATUS_SEC, /* sector protect */
    FLASH_X25_STATUS_SRP0       = 1UL << 7,             /* status register protection bit-0 */
    /* extended status2 bits */
    FLASH_X25_STATUS_SRP1       = 1UL << 8,             /* status register protection bit-1 */
    FLASH_X25_STATUS_SRL        = 1UL << 8,             /* status register lock */
    FLASH_X25_STATUS_QE         = 1UL << 9,             /* quad enable */
    FLASH_X25_STATUS_SUS2       = 1UL << 10,            /* suspend bit 2 */
    FLASH_X25_STATUS_EP_FAIL    = 1UL << 10,            /* Erase/Program Fail  */
    FLASH_X25_STATUS_LB0        = 1UL << 10,            /* lock security register 0 (не во всех, пересечение с EP_FAIL) */
    FLASH_X25_STATUS_LB1        = 1UL << 11,            /* lock security register 1 */
    FLASH_X25_STATUS_LB2        = 1UL << 12,            /* lock security register 2 */
    FLASH_X25_STATUS_LB3        = 1UL << 13,            /* lock security register 3 */
    FLASH_X25_STATUS_CMP        = 1UL << 14,            /* complement block protection */
    FLASH_X25_STATUS_SUS1       = 1UL << 15,            /* suspend bit 1 */
    /* extended status3 bits */
    FLASH_X25_STATUS_LPE        = 1UL << 18,            /* Low Power Enable */
    FLASH_X25_STATUS_WPS        = 1UL << 18,            /* Write protect selection */
    FLASH_X25_STATUS_HFQ        = 1UL << 20,            /* High Frequency Enable Bit */
    FLASH_X25_STATUS_DRV2       = 3UL << 21,            /* Output Driver Strength 2 */
    FLASH_X25_STATUS_DRV1       = 3UL << 22,            /* Output Driver Strength 1 */
    FLASH_X25_STATUS_HRSW       = 1UL << 23,            /* HOLD/RESET selection */
} t_flash_FLASH_X25_status;


t_flash_errs flash_x25_status_write_en(t_flash_iface *iface, unsigned flags);

t_flash_errs flash_x25_get_status16_2cmd(t_flash_iface *iface, unsigned *status);
t_flash_errs flash_x25_get_status24_3cmd(t_flash_iface *iface, unsigned *status);

t_flash_errs flash_x25_set_status16_1cmd(t_flash_iface *iface, unsigned status, unsigned flags);
t_flash_errs flash_x25_set_status24_1cmd(t_flash_iface *iface, unsigned status, unsigned flags);
t_flash_errs flash_x25_set_status24_3cmd(t_flash_iface *iface, unsigned status, unsigned flags);

t_flash_errs flash_x25_read_uid(t_flash_iface *iface, unsigned char *uid);



#endif // FLASH_DEV_FLASH_X25_H

