#include "flash_dev_xt25f32.h"

#define XT25F32_UID_SFDP_ADDR            0x000194


static const t_flash_erase_cmd f_xt25f32_erase_cmds[] = {
    {FLASH_CMD_CODE_ERASE_CHIP,    XT25F32_FLASH_SIZE,  XT25F32_ERASE_CHIP_TIME_MS, FLASH_ERASE_CMD_FLAG_NOADDR},
    {FLASH_CMD_CODE_ERASE_64K,                64*1024,  XT25F32_ERASE_64K_TIME_MS,  0},
    {FLASH_CMD_CODE_ERASE_32K,                32*1024,  XT25F32_ERASE_32K_TIME_MS,  0},
    {FLASH_CMD_CODE_ERASE_4K,                    4096,  XT25F32_ERASE_4K_TIME_MS,   0},
    {0,0,0,0}
};



const t_flash_info flash_info_xt25f32 =  {
    {XT25F32_ID_MANUFACTURER, XT25F32_ID_DEVICE, XT25F32_ID_CAPACITY, FLASH_ID_EDI_LEN_NOT_SUPPORTED},
    XT25F32_FLASH_SIZE,
    {FLASH_CMD_CODE_READ_STATUS, FLASH_X25_STATUS_BSY, 0x0, 0x0},
    {FLASH_CMD_CODE_READ, 0},
    {f_xt25f32_erase_cmds, FLASH_CMD_CODE_WRITE_ENABLE},
    {XT25F32_PAGE_SIZE, FLASH_CMD_CODE_PAGE_PROGRAM, XT25F32_PAGE_PROG_TIME_US},
    {XT25F32_UID_BITLEN},
    {XT25F32_STATUS_LEN, XT25F32_STATUS_MASK, XT25F32_STATUS_WR_TIME_MS},
    {   NULL,
        flash_xt25f32_get_status,
        flash_xt25f32_read,
        flash_xt25f32_erase,
        flash_xt25f32_get_erase_cmd,
        flash_xt25f32_write,
        flash_xt25f32_write_enable,
        NULL,
        NULL,
        NULL,
        NULL,

        NULL,
        NULL,
        NULL,

        flash_xt25f32_read_uid,
        flash_xt25f32_get_status_ex,
        flash_xt25f32_set_status_ex
    },
    NULL
};



t_flash_errs flash_xt25f32_read_uid(t_flash_iface *iface, unsigned char *uid) {
    return flash_exec_cmd_read_sfdp(iface, XT25F32_UID_SFDP_ADDR, uid, XT25F32_UID_BITLEN/8, FLASH_FLAGS_FLUSH);
}
