#ifndef FLASH_DEV_XT25F32_H
#define FLASH_DEV_XT25F32_H

#include "flash_dev_x25.h"

#ifdef __cplusplus
extern "C" {
#endif

#define XT25F32_ID_MANUFACTURER         0x0B
#define XT25F32_ID_DEVICE               0x40
#define XT25F32_ID_CAPACITY             0x16

#define XT25F32_FLASH_SIZE              (4*1024*1024)
#define XT25F32_PAGE_SIZE               256
#define XT25F32_SECTOR_SIZE             4096


#define XT25F32_UID_BITLEN               128

#define XT25F32_ERASE_CHIP_TIME_MS     30000
#define XT25F32_ERASE_64K_TIME_MS       1600
#define XT25F32_ERASE_32K_TIME_MS       1200
#define XT25F32_ERASE_4K_TIME_MS         800
#define XT25F32_PAGE_PROG_TIME_US        700
#define XT25F32_STATUS_WR_TIME_MS        800


#define XT25F32_STATUS_LEN                      2
#define XT25F32_STATUS_MASK                     (FLASH_X25_STATUS_BSY \
                                                | FLASH_X25_STATUS_WEL \
                                                | FLASH_X25_STATUS_BP0 \
                                                | FLASH_X25_STATUS_BP1 \
                                                | FLASH_X25_STATUS_BP2 \
                                                | FLASH_X25_STATUS_BP3 \
                                                | FLASH_X25_STATUS_BP4 \
                                                | FLASH_X25_STATUS_SRP0 \
                                                | FLASH_X25_STATUS_SRP1 \
                                                | FLASH_X25_STATUS_QE \
                                                | FLASH_X25_STATUS_LB0 \
                                                | FLASH_X25_STATUS_CMP \
                                                )


extern const t_flash_info flash_info_xt25f32;


#define flash_xt25f32_get_status                    flash_generic_get_status
#define flash_xt25f32_read                          flash_generic_read
#define flash_xt25f32_erase                         flash_generic_erase
#define flash_xt25f32_write                         flash_generic_write
#define flash_xt25f32_write_enable                  flash_generic_write_enable
#define flash_xt25f32_get_erase_cmd                 flash_generic_get_erase_cmd

#define flash_xt25f32_get_status_ex                 flash_x25_get_status16_2cmd
#define flash_xt25f32_set_status_ex                 flash_x25_set_status16_1cmd
t_flash_errs flash_xt25f32_read_uid(t_flash_iface *iface, unsigned char *uid);

#ifdef __cplusplus
}
#endif

#endif // FLASH_DEV_XT25F32_H
