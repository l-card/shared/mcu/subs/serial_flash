#include "flash_dev_z25vq32.h"

static const t_flash_erase_cmd f_z25vq32_erase_cmds[] = {
    {FLASH_CMD_CODE_ERASE_CHIP,    Z25VQ32_FLASH_SIZE,  Z25VQ32_ERASE_CHIP_TIME_MS, FLASH_ERASE_CMD_FLAG_NOADDR},
    {FLASH_CMD_CODE_ERASE_64K,                64*1024,  Z25VQ32_ERASE_64K_TIME_MS,  0},
    {FLASH_CMD_CODE_ERASE_32K,                32*1024,  Z25VQ32_ERASE_32K_TIME_MS,  0},
    {FLASH_CMD_CODE_ERASE_4K,                    4096,  Z25VQ32_ERASE_4K_TIME_MS,   0},
    {0,0,0,0}
};



const t_flash_info flash_info_z25vq32 =  {
    {Z25VQ32_ID_MANUFACTURER, Z25VQ32_ID_DEVICE, Z25VQ32_ID_CAPACITY, FLASH_ID_EDI_LEN_NOT_SUPPORTED},
    Z25VQ32_FLASH_SIZE,
    {FLASH_CMD_CODE_READ_STATUS, FLASH_X25_STATUS_BSY, 0x0, 0x0},
    {FLASH_CMD_CODE_READ, 0},
    {f_z25vq32_erase_cmds, FLASH_CMD_CODE_WRITE_ENABLE},
    {Z25VQ32_PAGE_SIZE, FLASH_CMD_CODE_PAGE_PROGRAM, Z25VQ32_PAGE_PROG_TIME_US},
    {Z25VQ32_UID_BITLEN},
    {Z25VQ32_STATUS_LEN, Z25VQ32_STATUS_MASK, Z25VQ32_STATUS_WR_TIME_MS},
    {   NULL,
        flash_z25vq32_get_status,
        flash_z25vq32_read,
        flash_z25vq32_erase,
        flash_z25vq32_get_erase_cmd,
        flash_z25vq32_write,
        flash_z25vq32_write_enable,
        NULL,
        NULL,
        NULL,
        NULL,

        NULL,
        NULL,
        NULL,

        flash_z25vq32_read_uid,
        flash_z25vq32_get_status_ex,
        flash_z25vq32_set_status_ex
    },
    NULL
};

