#ifndef FLASH_DEV_Z25VQ32_H
#define FLASH_DEV_Z25VQ32_H


#include "flash_dev_x25.h"

#ifdef __cplusplus
extern "C" {
#endif

#define Z25VQ32_ID_MANUFACTURER         0x5E
#define Z25VQ32_ID_DEVICE               0x40
#define Z25VQ32_ID_CAPACITY             0x16

#define Z25VQ32_FLASH_SIZE              (4*1024*1024)
#define Z25VQ32_PAGE_SIZE               256
#define Z25VQ32_SECTOR_SIZE             4096

#define Z25VQ32_UID_BITLEN              64

#define Z25VQ32_ERASE_CHIP_TIME_MS      (50*1000)
#define Z25VQ32_ERASE_64K_TIME_MS        (2*1000)
#define Z25VQ32_ERASE_32K_TIME_MS            800
#define Z25VQ32_ERASE_4K_TIME_MS             400
#define Z25VQ32_PAGE_PROG_TIME_US        (2*1000)
#define Z25VQ32_STATUS_WR_TIME_MS            100

#define Z25VQ32_STATUS_LEN                     3
#define Z25VQ32_STATUS_MASK                    (FLASH_X25_STATUS_BSY \
                                                | FLASH_X25_STATUS_WEL \
                                                | FLASH_X25_STATUS_BP0 \
                                                | FLASH_X25_STATUS_BP1 \
                                                | FLASH_X25_STATUS_BP2 \
                                                | FLASH_X25_STATUS_BP3 \
                                                | FLASH_X25_STATUS_BP4 \
                                                | FLASH_X25_STATUS_SRP0 \
                                                | FLASH_X25_STATUS_SRP1 \
                                                | FLASH_X25_STATUS_QE \
                                                | FLASH_X25_STATUS_LB1 \
                                                | FLASH_X25_STATUS_LB2 \
                                                | FLASH_X25_STATUS_LB3 \
                                                | FLASH_X25_STATUS_CMP \
                                                | FLASH_X25_STATUS_SUS1 \
                                                | FLASH_X25_STATUS_HFQ \
                                                | FLASH_X25_STATUS_DRV2 \
                                                | FLASH_X25_STATUS_HRSW \
                                                )




extern const t_flash_info flash_info_z25vq32;


#define flash_z25vq32_get_status     flash_generic_get_status
#define flash_z25vq32_read           flash_generic_read
#define flash_z25vq32_erase          flash_generic_erase
#define flash_z25vq32_write          flash_generic_write
#define flash_z25vq32_write_enable   flash_generic_write_enable
#define flash_z25vq32_get_erase_cmd  flash_generic_get_erase_cmd

#define flash_z25vq32_get_status_ex  flash_x25_get_status24_3cmd
#define flash_z25vq32_set_status_ex  flash_x25_set_status24_1cmd
#define flash_z25vq32_read_uid       flash_x25_read_uid

#ifdef __cplusplus
}
#endif


#endif // FLASH_DEV_Z25VQ32_H
