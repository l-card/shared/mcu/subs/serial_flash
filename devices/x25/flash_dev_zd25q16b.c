#include "flash_dev_zd25q16b.h"

static const t_flash_erase_cmd f_zd25q16b_erase_cmds[] = {
    {FLASH_CMD_CODE_ERASE_CHIP,    ZD25Q16B_FLASH_SIZE,  ZD25Q16B_ERASE_CHIP_TIME_MS, FLASH_ERASE_CMD_FLAG_NOADDR},
    {FLASH_CMD_CODE_ERASE_64K,                64*1024,  ZD25Q16B_ERASE_64K_TIME_MS,  0},
    {FLASH_CMD_CODE_ERASE_32K,                32*1024,  ZD25Q16B_ERASE_32K_TIME_MS,  0},
    {FLASH_CMD_CODE_ERASE_4K,                    4096,  ZD25Q16B_ERASE_4K_TIME_MS,   0},
    {0,0,0,0}
};



const t_flash_info flash_info_zd25q16b =  {
    {ZD25Q16B_ID_MANUFACTURER, ZD25Q16B_ID_DEVICE, ZD25Q16B_ID_CAPACITY, FLASH_ID_EDI_LEN_NOT_SUPPORTED},
    ZD25Q16B_FLASH_SIZE,
    {FLASH_CMD_CODE_READ_STATUS, FLASH_X25_STATUS_BSY, 0x0, 0x0},
    {FLASH_CMD_CODE_READ, 0},
    {f_zd25q16b_erase_cmds, FLASH_CMD_CODE_WRITE_ENABLE},
    {ZD25Q16B_PAGE_SIZE, FLASH_CMD_CODE_PAGE_PROGRAM, ZD25Q16B_PAGE_PROG_TIME_US},
    {ZD25Q16B_UID_BITLEN},
    {ZD25Q16B_STATUS_LEN, ZD25Q16B_STATUS_MASK, ZD25Q16B_STATUS_WR_TIME_MS},
    {   NULL,
        flash_zd25q16b_get_status,
        flash_zd25q16b_read,
        flash_zd25q16b_erase,
        flash_zd25q16b_get_erase_cmd,
        flash_zd25q16b_write,
        flash_zd25q16b_write_enable,
        NULL,
        NULL,
        NULL,
        NULL,

        NULL,
        NULL,
        NULL,

        flash_zd25q16b_read_uid,
        flash_zd25q16b_get_status_ex,
        flash_zd25q16b_set_status_ex
    },
    NULL
};
