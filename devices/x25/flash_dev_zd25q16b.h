#ifndef FLASH_DEV_ZD25Q16B_H
#define FLASH_DEV_ZD25Q16B_H

#include "flash_dev_x25.h"

#ifdef __cplusplus
extern "C" {
#endif

#define ZD25Q16B_ID_MANUFACTURER         0xBA
#define ZD25Q16B_ID_DEVICE               0x60
#define ZD25Q16B_ID_CAPACITY             0x15

#define ZD25Q16B_FLASH_SIZE              (2*1024*1024)
#define ZD25Q16B_PAGE_SIZE               256
#define ZD25Q16B_SECTOR_SIZE             4096

#define ZD25Q16B_UID_BITLEN              128

#define ZD25Q16B_ERASE_CHIP_TIME_MS      8
#define ZD25Q16B_ERASE_64K_TIME_MS       8
#define ZD25Q16B_ERASE_32K_TIME_MS       8
#define ZD25Q16B_ERASE_4K_TIME_MS        8
#define ZD25Q16B_PAGE_PROG_TIME_US       1600
#define ZD25Q16B_STATUS_WR_TIME_MS       4

#define ZD25Q16B_STATUS_LEN                     2
#define ZD25Q16B_STATUS_MASK                    (FLASH_X25_STATUS_BSY \
                                                | FLASH_X25_STATUS_WEL \
                                                | FLASH_X25_STATUS_BP0 \
                                                | FLASH_X25_STATUS_BP1 \
                                                | FLASH_X25_STATUS_BP2 \
                                                | FLASH_X25_STATUS_BP3 \
                                                | FLASH_X25_STATUS_BP4 \
                                                | FLASH_X25_STATUS_SRP0 \
                                                | FLASH_X25_STATUS_SRP1 \
                                                | FLASH_X25_STATUS_QE \
                                                | FLASH_X25_STATUS_LB0 \
                                                | FLASH_X25_STATUS_CMP \
                                                | FLASH_X25_STATUS_SUS1 \
                                                )




extern const t_flash_info flash_info_zd25q16b;


#define flash_zd25q16b_get_status     flash_generic_get_status
#define flash_zd25q16b_read           flash_generic_read
#define flash_zd25q16b_erase          flash_generic_erase
#define flash_zd25q16b_write          flash_generic_write
#define flash_zd25q16b_write_enable   flash_generic_write_enable
#define flash_zd25q16b_get_erase_cmd  flash_generic_get_erase_cmd

#define flash_zd25q16b_get_status_ex  flash_x25_get_status16_2cmd
#define flash_zd25q16b_set_status_ex  flash_x25_set_status16_1cmd
#define flash_zd25q16b_read_uid       flash_x25_read_uid

#ifdef __cplusplus
}
#endif


#endif // FLASH_DEV_ZD25Q16B_H
