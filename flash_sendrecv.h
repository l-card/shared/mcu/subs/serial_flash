#ifndef FLASH_SENDRECV_H
#define FLASH_SENDRECV_H

#include "flash.h"

/* Вариант реализации exec_cmd через отдельные функции send, recv, select и unselect */

typedef t_flash_errs (*t_flash_iface_send)(struct st_flash_iface *iface,
                                           const unsigned char* vals,
                                           size_t *snd_size);

typedef t_flash_errs (*t_flash_iface_recv)(struct st_flash_iface *iface,
                                           unsigned char* vals,
                                           size_t *rcv_size);


typedef t_flash_errs (*t_flash_iface_select)(struct st_flash_iface *iface, size_t total_size);
typedef t_flash_errs (*t_flash_iface_unselect)(struct st_flash_iface *iface);

typedef struct {
    t_flash_iface_send     send;    /**< Функция для передачи данных по интерфейсу */
    t_flash_iface_recv     recv;    /**< Функция приема данных по интерфейсу */
    t_flash_iface_select   select;  /**< Вызывается перед началом передачи команды */
    t_flash_iface_unselect unselect; /**< Вызывается после завершения передачи команды */
} t_flash_sendrecv_iface;


void flash_sendrecv_init(t_flash_iface *iface, const t_flash_sendrecv_iface *senrecv_iface);

t_flash_errs flash_sendrecv_exec_cmd(t_flash_iface *iface, const unsigned char *cmd, size_t cmd_size,
              const unsigned char *snd_data, size_t snd_size,
              unsigned char *resp, size_t resp_size, int flags);

#endif // FLASH_SENDRECV_H
