#include "../../flash.h"

/* инициализация интерфейса к flash через SPI для bf609.
   подразумевается при этом, что все настройки SPI выполнены вне данной
   функции */
t_flash_errs flash_iface_init_bf609_spi(t_flash_iface *iface, unsigned spi_port, unsigned cs_pin);
