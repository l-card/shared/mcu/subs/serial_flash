#ifndef FLASH_IFACE_IMX6_ECSPI_H
#define FLASH_IFACE_IMX6_ECSPI_H

#include "../../flash.h"
#include "spi/ecspi_ifc.h"

t_flash_errs flash_iface_init_imx6_ecspi(t_flash_iface *iface, dev_ecspi_e port, uint8_t cs, uint32_t clk);

#endif // FLASH_IFACE_IMX6_ECSPI_H
