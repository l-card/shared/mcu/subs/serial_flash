#ifndef FLASH_IFACE_LPC43XX_SSP_H
#define FLASH_IFACE_LPC43XX_SSP_H

#include "../../flash.h"

t_flash_errs flash_iface_init_lpc43xx_ssp(t_flash_iface *iface, int ssp_port, unsigned cs_pin);

#endif // FLASH_IFACE_LPC43XX_SSP_H
