#ifndef FLASH_IFACE_LTR_H
#define FLASH_IFACE_LTR_H

#include "../../flash.h"
#include "ltrapi.h"

t_flash_errs flash_iface_ltr_init(t_flash_iface *iface, TLTR *channel);
t_flash_errs flash_iface_ltr_set_channel(t_flash_iface *iface, TLTR *channel);
t_flash_errs flash_iface_ltr_close(t_flash_iface *iface);

INT flash_iface_ltr_conv_err(t_flash_errs err);



#endif // FLASH_IFACE_LPC43XX_SSP_H
