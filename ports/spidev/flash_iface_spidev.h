#ifndef FLASH_IFACE_SPIDEV_H
#define FLASH_IFACE_SPIDEV_H

/* Порт, использующий дполнительный репозиторий spi для доступа к SPI-интерфейсу
   микроконтроллеров */

#include "../../flash.h"
#include "spi.h"

#define FLASH_IFACE_SPIDEV_WRDLEN     8 /* Размер слова для работы с SPI устройствами.
                                           Именно оно должно быть установлено в t_spi_dev */


t_flash_errs flash_iface_spidev_init(t_flash_iface *iface, const t_spi_dev *dev);
t_flash_errs flash_iface_spidev_close(t_flash_iface *iface);

#endif // FLASH_IFACE_SPIDEV_H
